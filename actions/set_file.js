const MongoObjectId = require('mongodb').ObjectId
const distribution_config = require('../../distribution_config/conifg.json')
var cassandra_client = null

class SetFile {//costs $ 1.4/ million request /month
    static async insert_new_file(buffer, _owner_id=MongoObjectId(), life_time_seconds = 3156192000/*100 years approx*/){
        if(cassandra_client == null) throw Error("Datacenter not connected")
        const owner_id      = `${MongoObjectId(_owner_id)}`
        const time_added    = Math.floor(Date.now()/1000); //converting to seconds
        const time_expire   = time_added + Math.floor(life_time_seconds)
        const file_id       = `${distribution_config.server_id}${MongoObjectId()}`
        const query = `INSERT INTO file_store`
        + ` (id, owner_id, time_added, time_expire, buffer)`
        + ` VALUES ( '${file_id}', '${owner_id}', ${time_added}, ${time_expire}, ?);`
        const result = await cassandra_client.execute(query, [buffer])
        return {file_id, owner_id, time_added, time_expire}
    }

    static async expire_file_as_owner_safe( _owner_id, _file_id){
        //console.log(_file_id.toString().substr(0, 6), _file_id.toString().substr(6, 24));
        const file_id  = _file_id.toString().substr(0, 6)
                        + MongoObjectId(_file_id.toString().substr(6, 24)).toString()
        const time_expire = Math.floor(Date.now()/1000) + 30
        const owner_id   = `${MongoObjectId(_owner_id)}`
        const query = `UPDATE file_store`
        + ` SET time_expire = ${time_expire}`
        + ` WHERE id = '${file_id}'`
        + ` IF owner_id = '${owner_id}'`
        console.log(query);
        const result = await cassandra_client.execute(query)
        return result.info.achievedConsistency
    }

    static async expire_file_as_public_safe(_file_id){
        //console.log(_file_id.toString().substr(0, 6), _file_id.toString().substr(6, 24));
        const file_id  = _file_id.toString().substr(0, 6)
                        + MongoObjectId(_file_id.toString().substr(6, 24)).toString()
        const time_expire = Math.floor(Date.now()/1000) + 30
        const query = `UPDATE file_store`
        + ` SET time_expire = ${time_expire}`
        + ` WHERE id = '${file_id}'`
        + ` IF EXISTS`
        console.log(query);
        const result = await cassandra_client.execute(query)
        return result.info.achievedConsistency
    }
}

module.exports = ( (_cassandra_client)=>{
    cassandra_client = _cassandra_client
    return SetFile
})