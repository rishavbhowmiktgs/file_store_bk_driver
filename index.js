const cassandra = require('cassandra-driver')
const client = new cassandra.Client({
  contactPoints:['127.0.0.1:9042'],
  localDataCenter:"datacenter1",
  credentials: { username: 'cassandra', password: 'cassandra' },
  keyspace: 'ks1'
});

const SetFile = require('./actions/set_file')(client)
const GetFile = require('./actions/get_file')(client)

module.exports = {SetFile, GetFile}
/*adding new file
require('fs').readFile('C:/Users/risha/OneDrive/Desktop/Graph-Databases-For-Dummies.pdf', (err, data)=>{
  if(err) throw err;
  SetFile.insert_new_file(data)
  .then((result)=>{
    console.log('result',result);
  })
  .catch((err)=>{
    console.error(err)
  })
})
*/
/* get file
GetFile.select_file_as_owner('5f912b9640de520ec4e52461', 'FS00015f912b9640de520ec4e52462')
.then((result)=>{
  console.log('result',result.buffer.toString());
})
.catch((err)=>{
  console.error(err)
})
GetFile.select_file_as_public('FS00015f9135ca2ae30056188afc89')
.then((result)=>{
  console.log('PB result',result.buffer.length);
  require('fs').writeFileSync('pdf_file.pdf', result.buffer)
})
.catch((err)=>{
  console.error(err)
})
*/
/*
const dq = `DELETE FROM file_store WHERE time_added>1234;`
const id = `INSERT INTO file_store
(id, owner_id, time_added, time_expire)
VALUES
( '12345676', '12', 12345678, ${Date.now()+10000});`
const sq = `SELECT id FROM file_store WHERE time_expire>12345 ALLOW FILTERING;`
main()
async function main(){
  var results = await client.execute(sq)
  console.log(results);
  const set = []; results.rows.forEach((id)=>{set.push(`'${id.id}'`)})
  console.log(set);
  //client.execute(`DELETE FROM file_store WHERE id IN (${set.join()});`)
  /*results = await client.execute(`CREATE TABLE file_store
  ( id TEXT, owner_id TEXT, time_added BIGINT, time_expire BIGINT, buffer BLOB, PRIMARY KEY (id));`)
  console.log(results.rows);
  
}
*//*
SetFile.expire_file_as_public_safe('FS00015f912b9640de520ec4e52462')
.then((result)=>{
  console.log('result',result);
})
.catch((err)=>{
  console.error(err)
})*/
SetFile.expire_file_as_owner_safe( '5f912b9640de520ec4e52461', 'FS00015f912b9640de520ec4e52462')
.then((result)=>{
  console.log('result',result);
})
.catch((err)=>{
  console.error(err)
})